package com.szypula.currencyexchange.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.inOrder
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.szypula.currencyexchange.domain.ExchangeRatesInteractor
import com.szypula.currencyexchange.domain.ExchangeRatesState
import com.szypula.currencyexchange.domain.ExchangeRatesState.Error
import com.szypula.currencyexchange.domain.ExchangeRatesState.Loading
import com.szypula.currencyexchange.domain.ExchangeRatesState.Value
import com.szypula.currencyexchange.domain.exchangeRates
import com.szypula.currencyexchange.viewmodel.mapper.StateToViewStateMapper
import io.reactivex.Observable
import io.reactivex.schedulers.TestScheduler
import io.reactivex.subjects.PublishSubject
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.util.concurrent.TimeUnit.MILLISECONDS

class CurrenciesViewModelTest {

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private val refreshRateInMillis: Long = 10
    private val observerMock = mock<Observer<in ViewState>>()
    private val testScheduler = TestScheduler()
    private val testSubject = PublishSubject.create<ExchangeRatesState>()

    private val fakeGetExchangeRatesInteractor: ExchangeRatesInteractor = FakeGetExchangeRatesInteractor(testSubject)

    private val errorViewState = ViewStateBuilder()
        .withIsError(true)
        .build()
    private val loadingViewState = ViewStateBuilder()
        .withIsLoading(true)
        .build()
    private val currenciesViewState = viewState()
    private val stateToViewStateMapperMock: StateToViewStateMapper = mock {
        on { map(any<Error>(), any()) } doReturn errorViewState
        on { map(any<Loading>(), any()) } doReturn loadingViewState
        on { map(any<Value>(), any()) } doReturn currenciesViewState
    }

    private val tested = CurrenciesViewModel(
        refreshRateInMillis,
        "ANY",
        1.0,
        fakeGetExchangeRatesInteractor,
        stateToViewStateMapperMock,
        testScheduler,
        testScheduler
    )

    @Before
    fun setUp() {
        tested.viewState.observeForever(observerMock)
        MutableLiveData<UIEvent>()
            .apply { observeForever(tested) }
            .postValue(UIEvent.CurrenciesShown)
    }

    @Test
    fun `emits view state with error when source throws an error`() {
        testSubject.onError(RuntimeException())

        testScheduler.advanceTimeBy(refreshRateInMillis, MILLISECONDS)

        verify(observerMock).onChanged(errorViewState)
    }

    @Test
    fun `emits value every refreshRate even when source emits faster`() {
        repeatEvery(refreshRateInMillis / 10) {
            testSubject.onNext(Value(exchangeRates()))
        }

        testScheduler.advanceTimeBy(refreshRateInMillis * 3, MILLISECONDS)

        verify(observerMock, times(3)).onChanged(currenciesViewState)
    }

    @Test
    fun `emits loading state when source emits slower`() {
        repeatEvery(refreshRateInMillis * 10) {
            testSubject.onNext(Value(exchangeRates()))
        }

        testScheduler.advanceTimeBy(refreshRateInMillis * 3, MILLISECONDS)

        inOrder(observerMock) {
            verify(observerMock, times(1)).onChanged(currenciesViewState)
            verify(observerMock, times(2)).onChanged(loadingViewState)
        }
    }

    private fun repeatEvery(periodInMillis: Long, block: () -> Unit) {
        Observable
            .interval(0, periodInMillis, MILLISECONDS, testScheduler)
            .doOnNext { block() }
            .subscribe()
    }
}

class FakeGetExchangeRatesInteractor(
    private val observable: Observable<ExchangeRatesState>
) : ExchangeRatesInteractor {

    override fun lastExchangeRatesValue(baseCurrency: String, baseValue: Double): Value? = null

    override fun observeExchangeRate(baseCurrency: String, baseValue: Double): Observable<ExchangeRatesState> =
        observable
}