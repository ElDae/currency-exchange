package com.szypula.currencyexchange.repository

import com.szypula.currencyexchange.domain.ExchangeRatesBuilder
import com.szypula.currencyexchange.domain.RateBuilder
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Rule
import org.junit.Test
import retrofit2.HttpException
import java.net.SocketTimeoutException
import java.util.concurrent.TimeUnit

class NetworkExchangeRatesFetcherTest {

    @Rule
    @JvmField
    val server = MockWebServer()

    private val baseCurrency = "EUR"
    private val timeoutInMilliseconds = 1000L
    private val currenciesAPI = RatesAPIFactory.getRatesAPI(
        server.url("/").toString(),
        timeoutInMilliseconds
    )
    private val accessKey = "secret"

    private val tested = NetworkExchangeRatesFetcher(currenciesAPI, accessKey)

    @Test
    fun `maps currencies to rates`() {
        server.enqueue(
            MockResponse().setBody(
                """
                {
                  "base":"EUR",
                  "date":"2018-09-06",
                  "rates":{
                     "AUD":1.6191,
                     "BGN":1.959
                  }
                }""".trimIndent()
            )
        )

        tested.fetchExchangeRates(baseCurrency)
            .test()
            .assertValue(
                ExchangeRatesBuilder().withBase("EUR")
                    .withRates(
                        RateBuilder().withName("AUD").withValue(1.6191).build(),
                        RateBuilder().withName("BGN").withValue(1.959).build()
                    )
                    .build()
            )
            .assertComplete()
    }

    @Test
    fun `propagates exceptions as errors`() {
        server.enqueue(MockResponse().setResponseCode(500))

        tested.fetchExchangeRates(baseCurrency)
            .test()
            .assertError(HttpException::class.java)
    }

    @Test
    fun `completes without error for response delayed less than timeout`() {
        server.enqueue(
            MockResponse()
                .setBody(
                    """
                    {
                      "base":"ANY",
                      "rates":{}
                    }""".trimIndent()
                )
                .setBodyDelay(500, TimeUnit.MILLISECONDS)
                .setHeadersDelay(500, TimeUnit.MILLISECONDS)
        )

        tested.fetchExchangeRates(baseCurrency)
            .test()
            .assertNoErrors()
            .assertComplete()
    }

    @Test
    fun `propagates exception when timeout occurs`() {
        server.enqueue(
            MockResponse()
                .setBody("")
                .setBodyDelay(timeoutInMilliseconds, TimeUnit.MILLISECONDS)
                .setHeadersDelay(timeoutInMilliseconds, TimeUnit.MILLISECONDS)
        )

        tested.fetchExchangeRates(baseCurrency)
            .test()
            .assertError(SocketTimeoutException::class.java)
    }
}