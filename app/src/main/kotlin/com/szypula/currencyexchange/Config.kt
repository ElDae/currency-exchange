package com.szypula.currencyexchange

data class Config(
    val uiRefreshTimeInMillis: Long,
    val defaultBaseCurrency: String,
    val defaultBaseValue: Double,
    val endpoint: String,
    val accessKey: String,
    val requestTimeoutInMillis: Long,
    val requestIntervalInMillis: Long
)