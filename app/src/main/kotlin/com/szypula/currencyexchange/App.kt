package com.szypula.currencyexchange

import android.app.Application

class App: Application() {

    lateinit var config: Config

    override fun onCreate() {
        super.onCreate()

        config = Config(
            uiRefreshTimeInMillis = 1000L,
            defaultBaseCurrency = "EUR",
            defaultBaseValue = 1.0,
            endpoint = "http://data.fixer.io",
            accessKey = BuildConfig.ACCESS_KEY,
            requestTimeoutInMillis = 3000L,
            requestIntervalInMillis = 800L
        )
    }
}