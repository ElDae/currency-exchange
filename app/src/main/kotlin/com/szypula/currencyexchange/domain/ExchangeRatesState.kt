package com.szypula.currencyexchange.domain

sealed class ExchangeRatesState {

    object Loading : ExchangeRatesState()

    data class Value(val exchangeRates: ExchangeRates) : ExchangeRatesState()

    data class Error(val throwable: Throwable) : ExchangeRatesState()
}