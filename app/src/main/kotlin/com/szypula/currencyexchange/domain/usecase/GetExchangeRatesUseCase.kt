package com.szypula.currencyexchange.domain.usecase

import com.szypula.currencyexchange.domain.ExchangeRatesState
import io.reactivex.Observable

interface GetExchangeRatesUseCase {

    fun observeState(baseCurrency: String): Observable<ExchangeRatesState>
}