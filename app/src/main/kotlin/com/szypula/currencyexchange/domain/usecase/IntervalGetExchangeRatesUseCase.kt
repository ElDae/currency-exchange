package com.szypula.currencyexchange.domain.usecase

import com.szypula.currencyexchange.domain.ExchangeRatesState
import com.szypula.currencyexchange.domain.ExchangeRatesState.Loading
import com.szypula.currencyexchange.domain.ExchangeRatesState.Value
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit.MILLISECONDS

class IntervalGetExchangeRatesUseCase(
    private val exchangeRatesFetcher: ExchangeRatesFetcher,
    private val intervalInMilliseconds: Long,
    private val computationScheduler: Scheduler = Schedulers.computation(),
    private val ioScheduler: Scheduler = Schedulers.io()
) : GetExchangeRatesUseCase {

    override fun observeState(baseCurrency: String): Observable<ExchangeRatesState> =
        makeRequestEveryInterval(baseCurrency)
            .retryWhen { throwable -> throwable.delay(intervalInMilliseconds, MILLISECONDS, computationScheduler) }
            .map<ExchangeRatesState>(::Value)
            .startWith(Loading)

    private fun makeRequestEveryInterval(baseCurrency: String) = Observable
        .interval(0, intervalInMilliseconds, MILLISECONDS, computationScheduler)
        .observeOn(ioScheduler)
        .flatMap {
            exchangeRatesFetcher.fetchExchangeRates(baseCurrency)
                .subscribeOn(ioScheduler)
                .toObservable()
        }
}