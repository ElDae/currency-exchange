package com.szypula.currencyexchange.domain.usecase

import com.szypula.currencyexchange.domain.ExchangeRates
import io.reactivex.Single

interface ExchangeRatesFetcher {

    fun fetchExchangeRates(base: String): Single<ExchangeRates>
}