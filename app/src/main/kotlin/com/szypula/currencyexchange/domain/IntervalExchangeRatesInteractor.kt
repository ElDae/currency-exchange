package com.szypula.currencyexchange.domain

import com.szypula.currencyexchange.domain.ExchangeRatesState.Value
import com.szypula.currencyexchange.domain.usecase.GetExchangeRatesUseCase
import io.reactivex.Observable

class IntervalExchangeRatesInteractor(
    private val getExchangeRatesUseCase: GetExchangeRatesUseCase
) : ExchangeRatesInteractor {

    private var lastExchangeRates: ExchangeRates? = null

    override fun lastExchangeRatesValue(baseCurrency: String, baseValue: Double): Value? =
        lastExchangeRates?.let { exchangeRates ->
            Value(
                exchangeRates
                    .changeBaseCurrency(baseCurrency)
                    .calculateExchangeRates(baseValue)
            )
        }

    override fun observeExchangeRate(baseCurrency: String, baseValue: Double): Observable<ExchangeRatesState> =
        getExchangeRatesUseCase
            .observeState(baseCurrency)
            .doOnNext(this::cacheLastValue)
            .map { calculateExchangeRates(it, baseValue) }

    private fun calculateExchangeRates(
        exchangeRatesState: ExchangeRatesState,
        baseValue: Double
    ): ExchangeRatesState =
        if (exchangeRatesState is Value) {
            Value(exchangeRatesState.exchangeRates.calculateExchangeRates(baseValue))
        } else {
            exchangeRatesState
        }

    private fun cacheLastValue(exchangeRatesState: ExchangeRatesState?) {
        if (exchangeRatesState is Value) {
            lastExchangeRates = exchangeRatesState.exchangeRates
        }
    }
}