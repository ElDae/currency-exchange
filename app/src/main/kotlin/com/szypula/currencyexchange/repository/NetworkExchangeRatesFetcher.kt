package com.szypula.currencyexchange.repository

import com.szypula.currencyexchange.domain.ExchangeRates
import com.szypula.currencyexchange.domain.Rate
import com.szypula.currencyexchange.domain.usecase.ExchangeRatesFetcher
import com.szypula.currencyexchange.repository.model.RatesResponse
import io.reactivex.Single

class NetworkExchangeRatesFetcher(
    private val ratesAPI: RatesAPI,
    private val accessKey: String
) : ExchangeRatesFetcher {

    override fun fetchExchangeRates(base: String): Single<ExchangeRates> =
        ratesAPI
            .fetchLatestRates(base, accessKey)
            .map(::toExchangeRates)

    private fun toExchangeRates(response: RatesResponse) =
        ExchangeRates(
            response.base,
            response.rates.map(::toRate)
        )

    private fun toRate(entry: Map.Entry<String, Double>) =
        Rate(
            entry.key,
            entry.value
        )
}