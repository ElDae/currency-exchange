package com.szypula.currencyexchange.repository

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit.MILLISECONDS

object RatesAPIFactory {

    fun getRatesAPI(
        baseUrl: String,
        timeoutInMilliseconds: Long
    ): RatesAPI {
        val okHttpClient = OkHttpClient
            .Builder()
            .readTimeout(timeoutInMilliseconds, MILLISECONDS)
            .addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
            .build()

        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
            .create(RatesAPI::class.java)
    }
}