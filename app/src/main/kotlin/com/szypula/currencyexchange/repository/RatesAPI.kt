package com.szypula.currencyexchange.repository

import com.szypula.currencyexchange.repository.model.RatesResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RatesAPI {

    @GET("/api/latest")
    fun fetchLatestRates(
        @Query("base") base: String,
        @Query("access_key") accessKey: String
    ): Single<RatesResponse>
}