package com.szypula.currencyexchange.repository.model

data class RatesResponse(
    val base: String,
    val date: String,
    val rates: Map<String, Double>
)

