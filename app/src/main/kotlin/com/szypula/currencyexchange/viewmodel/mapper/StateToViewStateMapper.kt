package com.szypula.currencyexchange.viewmodel.mapper

import com.szypula.currencyexchange.domain.ExchangeRatesState
import com.szypula.currencyexchange.viewmodel.ViewState

interface StateToViewStateMapper {

    fun map(state: ExchangeRatesState, value: String): ViewState
}