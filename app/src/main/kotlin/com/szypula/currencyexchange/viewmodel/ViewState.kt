package com.szypula.currencyexchange.viewmodel

data class ViewState(
    val currencies: List<Currency>,
    val isLoading: Boolean,
    val isError: Boolean
) {

    companion object {

        val EMPTY = ViewState(
            currencies = emptyList(),
            isLoading = false,
            isError = false
        )
    }
}

data class Currency(
    val name: String,
    val value: String
)