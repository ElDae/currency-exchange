package com.szypula.currencyexchange.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.szypula.currencyexchange.Config
import com.szypula.currencyexchange.domain.IntervalExchangeRatesInteractor
import com.szypula.currencyexchange.domain.usecase.IntervalGetExchangeRatesUseCase
import com.szypula.currencyexchange.repository.NetworkExchangeRatesFetcher
import com.szypula.currencyexchange.repository.RandomizingExchangeRatesFetcher
import com.szypula.currencyexchange.repository.RatesAPIFactory
import com.szypula.currencyexchange.viewmodel.mapper.CachingStateToViewStateMapper

class CurrenciesViewModelFactory(
    private val config: Config
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T = with(config) {
        val ratesAPI = RatesAPIFactory.getRatesAPI(endpoint, requestTimeoutInMillis)
        val exchangeRatesFetcher = RandomizingExchangeRatesFetcher(NetworkExchangeRatesFetcher(ratesAPI, accessKey))
        val getExchangeUseCase = IntervalGetExchangeRatesUseCase(exchangeRatesFetcher, requestIntervalInMillis)
        val exchangeRatesInteractor = IntervalExchangeRatesInteractor(getExchangeUseCase)
        val stateToViewStateMapper = CachingStateToViewStateMapper()

        return CurrenciesViewModel(
            uiRefreshTimeInMillis,
            defaultBaseCurrency,
            defaultBaseValue,
            exchangeRatesInteractor,
            stateToViewStateMapper
        ) as T
    }
}