## Currency exchange app.

# Requirements:

- Show the list of currencies with their exchange rates.
- Update the exchange rate every 1 second.
- When taping on the currency slide the row to the top and select the input.
- After changing the amount, update values for other currencies.

# Important notes:

- This repository is an outcome of a job interview assignment. The endpoint has been changed to a private one. 
The original endpoint returned different values for every request. In order to simulate that I've added a 
RandomizingExchangeRatesFetcher class (see it's documentation for more details).
- The new endpoint is http://data.fixer.io and it requires an accessToken to work. It needs to be specified in the 
*local.properties* file like this:

```
accessKey="secret"
```

# About implementation:

- I made sure that the domain logic does not depend on other layers of the application just like it's prescribed by 
the clean architecture.
- Ui is very rudimentary, I focused more on the rest of the app.
- Using DI library seemed like an overkill for this. However since every object creation is in CurrenciesViewModelFactory it would be very easy to add it.

# Room for improvement:

- Tests for IntervalExchangeRatesInteractor and ExchangeRatesCalculator.
- Acceptance UI tests using Espresso.
- Handling of Error and Loading states.
- Proper logging (always useful).
- Data binding used in CurrencyListAdapter would save some of the boilerplate.
- Test for communication from the view to the CurrenciesViewModel.
- Some more work could have been offloaded to the background thread, for example ExchangeRatesInteractor.lastExchangeRatesValue.